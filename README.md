# docker-mongodb

**Starter Kit** do szybkiego uruchomienia bazy danych [MongoDB](https://www.mongodb.com) jako [kontener](https://hub.docker.com/_/mongo) Docker'a i panelu do zarządzania bazą danych [mongo-express](https://github.com/mongo-express/mongo-express) również jako [kontener](https://hub.docker.com/_/mongo-express).



## Skrypty

Pamiętaj że musisz mieć uprawnienia do uruchamiania polecenie `docker` które jest używane wewnątrz poniższych plików. Jeżeli konto użytkownika z którego korzystasz nie ma takich uprawnień to uruchom polecenia z użyciem **sudo**, np. `sudo ./mongodb_up.sh`

| Nazwa pliku            | Opis                                      |
| :--------------------- | :---------------------------------------- |
| `mongodb_up.sh`        | Uruchamia kontener z serwerem **MongoDB** |
| `mongodb_down.sh`      | Zatrzymuje serwer **MongoDB**             |
| `mongoexpress_up.sh`   | Uruchamia panel **mongo-express**         |
| `mongoexpress_down.sh` | Zatrzymuje panel **mongo-express**        |

### Kolejność uruchamiania

1. `sudo ./mongodb_up.sh`
2. `sudo ./mongoexpress_up.sh`

### Kolejność zamykania

1. `sudo ./mongoexpress_down.sh`
2. `sudo ./mongodb_down.sh`



## Baza danych

Baza jest dostępna pod adresem `127.0.0.1:27017`

- Login: **db_admin**
- Hasło: **db_pass**



## Panel

Panel administracyjny jest dostępny pod adresem http://127.0.0.1:8081

- Login: **me_admin**
- Hasło: **me_pass**



## Autor

Adrian Gargula