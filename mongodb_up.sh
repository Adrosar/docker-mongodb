#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker run \
    -d --rm -i -t \
    --name "mongo-db" \
    -e MONGO_INITDB_ROOT_USERNAME=db_admin \
    -e MONGO_INITDB_ROOT_PASSWORD=db_pass \
    -v "$CURRENT_DIR/data/db:/data/db" \
    -p 27017:27017 \
    mongo:4.2.7