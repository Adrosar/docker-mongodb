#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker run \
    -d --rm -i -t \
    --name "mongo-express" \
    -e ME_CONFIG_BASICAUTH_USERNAME=me_admin \
    -e ME_CONFIG_BASICAUTH_PASSWORD=me_pass \
    -e ME_CONFIG_MONGODB_ADMINUSERNAME=db_admin \
    -e ME_CONFIG_MONGODB_ADMINPASSWORD=db_pass \
    -e ME_CONFIG_MONGODB_SERVER="mongodb" \
    --link "mongo-db:mongodb" \
    -p 8081:8081 \
    mongo-express:0.54